package com.example.market.mapper;

import com.example.market.model.dto.userDTO.LoginResponseDTO;
import com.example.market.model.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    LoginResponseDTO toDTO(User user);
}
