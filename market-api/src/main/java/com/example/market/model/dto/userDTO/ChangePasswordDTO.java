package com.example.market.model.dto.userDTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class ChangePasswordDTO {
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    @NotBlank
    private String currentPassword;
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    @NotBlank
    private String newPassword;
}
