package com.example.market.model.entities;

import com.example.market.model.local.District;
import com.example.market.model.local.Province;
import com.example.market.model.local.Ward;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class User extends BaseEntities{
    private String fullName;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    private LocalDate dOB;
    private Double rate;
    private Double gold;
    private String street;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Province city;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Ward ward;

    @OneToMany
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Feedback> feedbackList;

    @OneToOne(mappedBy = "seller",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Transaction transactionSeller;

    @OneToOne(mappedBy = "customer",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Transaction transactionCustomer;

    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Account account;
}
