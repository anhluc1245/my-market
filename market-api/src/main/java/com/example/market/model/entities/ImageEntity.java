package com.example.market.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ImageEntity extends BaseEntities{
    private String filePath;
    private Boolean isMainImage;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Product product;
}
