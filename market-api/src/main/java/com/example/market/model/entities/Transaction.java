package com.example.market.model.entities;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Transaction extends BaseEntities{
    private Double price;

    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Product product;

    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User seller;

    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User customer;
}
