package com.example.market.model.appEnum;

public enum ProductStatusEnum {
    SOLD,
    AVAILABLE,
    CANCELED
}
