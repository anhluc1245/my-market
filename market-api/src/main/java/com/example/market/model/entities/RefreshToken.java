package com.example.market.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class RefreshToken extends BaseEntities{
    private String refreshToken;
    private Boolean revoked;

    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Account account;
}
