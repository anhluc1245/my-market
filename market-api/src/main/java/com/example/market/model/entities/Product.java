package com.example.market.model.entities;

import com.example.market.model.appEnum.ProductStatusEnum;
import com.example.market.model.local.District;
import com.example.market.model.local.Province;
import com.example.market.model.local.Ward;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Product extends BaseEntities{
    private String productName;
    @Column(length = 5000)
    private String description;
    private Double price;
    @Enumerated(EnumType.STRING)
    private ProductStatusEnum productStatusEnum;
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<ImageEntity> imageEntityList;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    private String street;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Province city;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Ward ward;

    @OneToMany
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Feedback> feedbackList;

    @OneToOne(mappedBy = "product",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Transaction transaction;
}
