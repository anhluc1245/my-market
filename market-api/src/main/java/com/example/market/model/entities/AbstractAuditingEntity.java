package com.example.market.model.entities;

import com.example.market.constant.DateTimeConstant;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity {
    @CreatedBy
    protected String createdBy;
    @CreatedDate
    @JsonFormat(pattern = DateTimeConstant.DATE_FORMAT_JSON)
    protected LocalDateTime createdDate;
    @LastModifiedBy
    protected String lastModifiedBy;

    @LastModifiedDate
    @JsonFormat(pattern = DateTimeConstant.DATE_FORMAT_JSON)
    protected LocalDateTime lastModifiedDate;
    protected Boolean deleted;

}
