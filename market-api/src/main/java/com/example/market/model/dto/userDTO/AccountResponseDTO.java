package com.example.market.model.dto.userDTO;

import com.example.market.model.appEnum.UserRoleEnum;
import lombok.Data;

@Data
public class AccountResponseDTO {
    private Long id;
    private String email;
    private Boolean active;
    private UserRoleEnum role;
}
