package com.example.market.model.dto.userDTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class RegisterRequestDTO {
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    @NotBlank
    private String email;
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    @NotBlank
    private String password;
    @NotBlank
    private String fullName;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    @NotBlank
    private String phoneNumber;
}
