package com.example.market.model.dto.addressDTO;

import lombok.Data;

@Data
public class WardResponseDTO {
    private Long id;
    private String name;

    public WardResponseDTO(Long id) {
        this.id = id;
    }
}
