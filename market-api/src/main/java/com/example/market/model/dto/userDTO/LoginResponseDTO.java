package com.example.market.model.dto.userDTO;

import com.example.market.model.dto.addressDTO.DistrictResponseDTO;
import com.example.market.model.dto.addressDTO.ProvinceResponseDTO;
import com.example.market.model.dto.addressDTO.WardResponseDTO;
import com.example.market.model.entities.Account;
import com.example.market.model.local.District;
import com.example.market.model.local.Province;
import com.example.market.model.local.Ward;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseDTO {
    private String accessToken;
    private String refreshToken;
    private Long id;
    private String fullName;
    private String phoneNumber;
    private ProvinceResponseDTO city;
    private DistrictResponseDTO district;
    private WardResponseDTO ward;
    private String street;
    private LocalDate dOB;
    private String imageProfile;
    private AccountResponseDTO account;
}
