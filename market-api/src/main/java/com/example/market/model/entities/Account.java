package com.example.market.model.entities;

import com.example.market.model.appEnum.UserRoleEnum;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import jakarta.validation.constraints.Pattern;
import org.hibernate.annotations.Where;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class Account extends BaseEntities{
    @Column(nullable = false)
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    private String password;
    private String resetToken;
    private Boolean active;
    @Enumerated(EnumType.STRING)
    private UserRoleEnum role;
    @OneToOne(mappedBy = "account",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private RefreshToken refreshToken;
    @OneToOne(mappedBy = "account",cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;
}
