package com.example.market.service.impl;

import com.example.market.model.entities.User;
import com.example.market.repository.UserRepository;
import com.example.market.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByAccountEmailAndDeletedFalse(String email) {
        return userRepository.findByAccountEmailAndDeletedFalse(email);
    }
}
