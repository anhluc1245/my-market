package com.example.market.service.impl;

import com.example.market.model.entities.Account;
import com.example.market.repository.AccountRepository;
import com.example.market.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public Boolean existAccountEmail(String email) {
        return accountRepository.existsAccountByEmailAndDeletedFalse(email);
    }

    @Override
    public Boolean existUserPhoneNumber(String phoneNumber) {
        return accountRepository.existsAccountByUserPhoneNumberAndDeletedFalse(phoneNumber);
    }

    @Override
    public Optional<Account> findAccountByEmailAndDeletedFalse(String email) {
        return accountRepository.findAccountByEmailAndDeletedFalse(email);
    }

    @Override
    public void save(Account account) {
        accountRepository.save(account);
    }
}
