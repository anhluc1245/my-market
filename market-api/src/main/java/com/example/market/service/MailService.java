package com.example.market.service;

public interface MailService {
    public void sendMail(String to, String subject, String text);
}
