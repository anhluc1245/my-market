package com.example.market.service.impl;

import com.example.market.mapper.UserMapper;
import com.example.market.model.appEnum.UserRoleEnum;
import com.example.market.model.dto.userDTO.LoginResponseDTO;
import com.example.market.model.dto.userDTO.RegisterRequestDTO;
import com.example.market.model.entities.Account;
import com.example.market.model.entities.RefreshToken;
import com.example.market.model.entities.User;
import com.example.market.security.TokenProvider;
import com.example.market.service.AccountService;
import com.example.market.service.AuthenticationService;
import com.example.market.service.TokenService;
import com.example.market.service.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final TokenProvider tokenProvider;
    private final TokenService tokenService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;

    public AuthenticationServiceImpl(AuthenticationManagerBuilder authenticationManagerBuilder, TokenProvider tokenProvider, TokenService tokenService, UserService userService, UserMapper userMapper, AccountService accountService, PasswordEncoder passwordEncoder) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.tokenProvider = tokenProvider;
        this.tokenService = tokenService;
        this.userService = userService;
        this.userMapper = userMapper;
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public LoginResponseDTO login(String email, String password) {
        try {
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(email,password);
            Authentication authentication =
                    authenticationManagerBuilder.getObject().authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            Optional<RefreshToken> refreshTokenOptional =
                    tokenService.findByAccountEmailAndDeletedFalse(email);
            String accessToken = tokenProvider.createAccessToken(authentication);
            String refreshToken = tokenProvider.createRefreshToken(authentication);
            refreshTokenOptional.get().setRefreshToken(refreshToken);
            refreshTokenOptional.get().setRevoked(false);
            tokenService.saveToken(refreshTokenOptional.get());
            return createNew(email,accessToken,refreshToken);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void createNew(RegisterRequestDTO registerRequestDTO) {
        Account account = new Account();
        account.setEmail(registerRequestDTO.getEmail());
        account.setPassword(passwordEncoder.encode(registerRequestDTO.getPassword()));
        account.setDeleted(false);
        account.setActive(false);
        account.setRole(UserRoleEnum.USER);
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setDeleted(false);
        refreshToken.setRevoked(true);
        User user = new User();
        user.setDeleted(false);
        user.setFullName(registerRequestDTO.getFullName());
        user.setPhoneNumber(registerRequestDTO.getPhoneNumber());
        user.setGold(0.0);
        account.setUser(user);
        account.setRefreshToken(refreshToken);
        user.setAccount(account);
        refreshToken.setAccount(account);
        accountService.save(account);
    }

    @Override
    public LoginResponseDTO refresh(String token) {
        try{
            Optional<RefreshToken> refreshTokenOptional =
                    tokenService.findByRefreshTokenAndRevokedFalseAndDeletedFalse(token);
            Authentication authentication =
                    tokenProvider.getAuthentication(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String newAccessToken = tokenProvider.createAccessToken(authentication);
            String newRefreshToken = tokenProvider.createRefreshToken(authentication);
            refreshTokenOptional.get().setRefreshToken(newRefreshToken);
            tokenService.saveToken(refreshTokenOptional.get());
            return createNew(refreshTokenOptional.get().getAccount().getEmail(),newAccessToken,newRefreshToken);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    private LoginResponseDTO createNew(String email, String accessToken, String refreshToken){
        Optional<User> userOptional =
                userService.findByAccountEmailAndDeletedFalse(email);
        LoginResponseDTO loginResponseDTO = userMapper.toDTO(userOptional.get());
        loginResponseDTO.setAccessToken(accessToken);
        loginResponseDTO.setRefreshToken(refreshToken);
        return loginResponseDTO;
    }
}
