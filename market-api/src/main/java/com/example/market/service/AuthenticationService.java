package com.example.market.service;

import com.example.market.model.dto.userDTO.LoginResponseDTO;
import com.example.market.model.dto.userDTO.RegisterRequestDTO;

public interface AuthenticationService {
    LoginResponseDTO login(String email, String password);
    void createNew(RegisterRequestDTO registerRequestDTO);
    LoginResponseDTO refresh(String token);
}
