package com.example.market.service.impl;

import com.example.market.model.entities.RefreshToken;
import com.example.market.repository.TokenRepository;
import com.example.market.service.TokenService;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class TokenServiceImpl implements TokenService {
    private final TokenRepository tokenRepository;

    @Override
    public void saveToken(RefreshToken refreshToken) {
        tokenRepository.save(refreshToken);
    }

    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public Optional<RefreshToken> findByAccountEmailAndDeletedFalse(String email) {
        return tokenRepository.findByAccountEmailAndDeletedFalse(email);
    }

    @Override
    public Optional<RefreshToken> findByRefreshTokenAndRevokedFalseAndDeletedFalse(String token) {
        return tokenRepository.findByRefreshTokenAndRevokedFalseAndDeletedFalse(token);
    }
}
