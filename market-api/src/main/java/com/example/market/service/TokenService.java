package com.example.market.service;

import com.example.market.model.entities.RefreshToken;

import java.util.Optional;

public interface TokenService {
    Optional<RefreshToken> findByAccountEmailAndDeletedFalse(String email);
    void saveToken(RefreshToken refreshToken);
    Optional<RefreshToken> findByRefreshTokenAndRevokedFalseAndDeletedFalse(String token);

}
