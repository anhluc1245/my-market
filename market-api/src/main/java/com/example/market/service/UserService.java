package com.example.market.service;

import com.example.market.model.entities.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findByAccountEmailAndDeletedFalse(String email);
    void save(User user);
}
