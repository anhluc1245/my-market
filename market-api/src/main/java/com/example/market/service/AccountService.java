package com.example.market.service;

import com.example.market.model.dto.userDTO.LoginResponseDTO;
import com.example.market.model.entities.Account;

import java.util.Optional;

public interface AccountService {
    Optional<Account> findAccountByEmailAndDeletedFalse(String email);
    Boolean existAccountEmail(String email);
    Boolean existUserPhoneNumber(String phoneNumber);
    void save(Account account);
}
