package com.example.market.security;

import com.example.market.exception.UnAuthorizedException;
import com.example.market.model.appEnum.UserRoleEnum;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.util.Objects;
import java.util.Optional;

@Component
public class SecurityUtils {
    public static Optional<String> getCurrentAccountOpt(){
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        Object object = authentication.getPrincipal();
        if (object instanceof User){
            return Optional.of(((User) object).getUsername());
        }
        return Optional.empty();
    }
    public static String getCurrentAccount(){
        return getCurrentAccountOpt().orElseThrow(UnAuthorizedException::new);
    }
    public static Optional<UserRoleEnum> getCurrentRoleOpt(){
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream()
                .map(grantedAuthority -> UserRoleEnum.valueOf(
                        grantedAuthority.getAuthority().replaceAll("ROLE_","")))
                .findFirst();
    }
    public static UserRoleEnum getCurrentRole(){
        return getCurrentRoleOpt().orElseThrow(UnAuthorizedException::new);
    }
    public static boolean isAdmin(){
        return getCurrentRole() == UserRoleEnum.ADMIN;
    }
    public static boolean isUser(){
        return getCurrentRole() == UserRoleEnum.USER;
    }
}
