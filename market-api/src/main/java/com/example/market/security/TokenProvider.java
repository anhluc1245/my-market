package com.example.market.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.SimpleIdGenerator;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TokenProvider {
    @Value("${app.token.signKey}")
    private String signingKey;
    public String createAccessToken(Authentication authentication){
        //Lây danh sách quyen của người dùng và nối chúng thành một chuỗi,
        String roles = authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.joining(","));
        LocalDateTime expiredTime = LocalDateTime.now().plusMinutes(30);
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim("roles",roles)
                .setExpiration(Date.from(expiredTime.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS256, signingKey)
                .compact();
    }
    public String createRefreshToken(Authentication authentication){
        String roles = authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.joining(","));
        LocalDateTime expiredTime = LocalDateTime.now().plusHours(24);
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim("roles",roles)
                .setExpiration(Date.from(expiredTime.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS256, signingKey)
                .compact();
    }
    public Authentication getAuthentication(String token){
        if (!StringUtils.hasText(token)){
            return null;
        }
        try {
            Claims claims =Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(token)
                    .getBody();
            List<GrantedAuthority> roles = Arrays.stream(claims.get("roles").toString().split(","))
                    .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
            User user = new User(claims.getSubject(),"",roles);
            return new UsernamePasswordAuthenticationToken(user,null,roles);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
