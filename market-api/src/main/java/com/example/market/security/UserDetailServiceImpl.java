package com.example.market.security;

import com.example.market.model.entities.User;
import com.example.market.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> userOptional =
                userRepository.findByAccountEmailAndDeletedFalse(email);
        if (userOptional.isEmpty()){
            throw new UsernameNotFoundException("User is not exist");
        }
        User user = userOptional.get();
        return new org.springframework.security.core.userdetails.User(
                user.getAccount().getEmail(),
                user.getAccount().getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_"+user.getAccount().getRole().name()))
        );
    }

}
