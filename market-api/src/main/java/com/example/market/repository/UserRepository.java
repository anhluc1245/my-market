package com.example.market.repository;

import com.example.market.model.entities.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User,Integer>{
    Optional<User> findByAccountEmailAndDeletedFalse(String email);
}
