package com.example.market.repository;

import com.example.market.model.entities.RefreshToken;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends BaseRepository<RefreshToken, Integer>{
    Optional<RefreshToken> findByAccountEmailAndDeletedFalse(String email);
    Optional<RefreshToken> findByRefreshTokenAndRevokedFalseAndDeletedFalse(String token);
}
