package com.example.market.repository;

import com.example.market.model.entities.Account;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends BaseRepository<Account , Integer>{
    Optional<Account> findAccountByEmailAndDeletedFalse(String email);
    Boolean existsAccountByEmailAndDeletedFalse(String email);
    Boolean existsAccountByUserPhoneNumberAndDeletedFalse(String phoneNumber);
}
