package com.example.market.config;

import com.example.market.security.SecurityUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class AppAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
       try {
            Optional<String> signing = SecurityUtils.getCurrentAccountOpt();
            if (signing.isPresent()){
                return signing;
            }
            return Optional.of("system");
       }catch (NullPointerException e){
           return Optional.of("system");
       }
    }
}
