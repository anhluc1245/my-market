package com.example.market.resource.publicResource;

import com.example.market.model.dto.userDTO.ChangePasswordDTO;
import com.example.market.model.entities.Account;
import com.example.market.security.SecurityUtils;
import com.example.market.security.TokenProvider;
import com.example.market.service.AccountService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class AccountResource {
    private final TokenProvider tokenProvider;
    private final SecurityUtils securityUtils;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;

    public AccountResource(TokenProvider tokenProvider, SecurityUtils securityUtils, AccountService accountService, PasswordEncoder passwordEncoder) {
        this.tokenProvider = tokenProvider;
        this.securityUtils = securityUtils;
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
    }

    @PutMapping("/change-password")
    private ResponseEntity<?> changePassword(@RequestBody @Valid ChangePasswordDTO passwordDTO){
        String email = SecurityUtils.getCurrentAccount();
        if (email==null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        if (passwordDTO==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Optional<Account> accountOptional =
                accountService.findAccountByEmailAndDeletedFalse(email);
        if (accountOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        String encodedPassword = accountOptional.get().getPassword();
        if (!passwordEncoder.matches(passwordDTO.getCurrentPassword(),encodedPassword)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        accountOptional.get().setPassword(passwordEncoder.encode(passwordDTO.getNewPassword()));
        accountService.save(accountOptional.get());
        return ResponseEntity.ok().build();
    }

}
