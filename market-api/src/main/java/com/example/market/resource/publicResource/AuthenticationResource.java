package com.example.market.resource.publicResource;

import com.example.market.component.SuccessResponse;
import com.example.market.model.appEnum.UserRoleEnum;
import com.example.market.model.dto.userDTO.LoginRequestDTO;
import com.example.market.model.dto.userDTO.LoginResponseDTO;
import com.example.market.model.dto.userDTO.RefreshTokenDTO;
import com.example.market.model.dto.userDTO.RegisterRequestDTO;
import com.example.market.model.entities.Account;
import com.example.market.security.SecurityUtils;
import com.example.market.service.AccountService;
import com.example.market.service.AuthenticationService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationResource {
    private final AccountService accountService;
    private final AuthenticationService authenticationService;

    public AuthenticationResource(AccountService accountService, AuthenticationService authenticationService) {
        this.accountService = accountService;
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    private ResponseEntity<?> login(@RequestBody @Valid LoginRequestDTO loginRequestDTO){
        Optional<Account> accountOptional =
                accountService.findAccountByEmailAndDeletedFalse(loginRequestDTO.getEmail());
        if (accountOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        if (accountOptional.get().getRole()!= UserRoleEnum.USER){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You need login by public user!");
        }
        LoginResponseDTO loginResponseDTO = authenticationService.login(
                loginRequestDTO.getEmail(), loginRequestDTO.getPassword());
        if (loginResponseDTO==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }
    @PostMapping("/register")
    private ResponseEntity<?> register(@RequestBody @Valid RegisterRequestDTO registerRequestDTO,
                                       BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        if (accountService.existAccountEmail(registerRequestDTO.getEmail())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Email is existed!");
        }
        if (accountService.existUserPhoneNumber(registerRequestDTO.getPhoneNumber())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("PhoneNumber is existed!");
        }
        authenticationService.createNew(registerRequestDTO);
        return ResponseEntity.ok(new SuccessResponse("Register success!"));
    }
    @PostMapping("/refresh")
    private ResponseEntity<?> refreshToken(@RequestBody @Valid RefreshTokenDTO refreshTokenDTO){
        if (refreshTokenDTO == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        LoginResponseDTO loginResponseDTO =
                    authenticationService.refresh(refreshTokenDTO.getRefreshToken());
        if (loginResponseDTO==null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }
}
